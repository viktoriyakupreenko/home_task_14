package com.epam.tat2015.home14.lib.feature.disk.screen;

import com.epam.tat2015.home14.lib.feature.disk.TextFile;
import com.epam.tat2015.home14.lib.ui.Browser;
import com.epam.tat2015.home14.lib.report.Logger;
import org.openqa.selenium.By;

import static com.epam.tat2015.home14.lib.feature.common.CommonConstants.*;


public class DiskTrashPage extends DiskBasePage {

    By TRASH_BOX_LOCATOR = By.xpath("//div[@data-id='/trash']/div");
    By DELETE_BUTTON_LOCATOR = By.xpath("//div[contains(@class,'visible')]//button[@data-click-action='resource.delete']");
    By YANDEX_DISK_LINK_LOCATOR = By.xpath("//a[@class='b-crumbs__root _link ns-action']");
    By RESTORE_BUTTON_LOCATOR = By.xpath("//button[@data-click-action='resource.restore']");
    By OPEN_TRASH__BUTTON_LOCATOR = By.xpath("//button[@data-click-action='listing.openFolder']");

    public void openTrash() {
        Logger.info("Open trash page");
        Browser.current().waitForElementIsClickable(TRASH_BOX_LOCATOR);
        Browser.current().click(TRASH_BOX_LOCATOR);
        Browser.current().waitForElementIsClickable(OPEN_TRASH__BUTTON_LOCATOR);
        Browser.current().click(OPEN_TRASH__BUTTON_LOCATOR);

        Browser.current().waitForDisappear(TRASH_BOX_LOCATOR);
    }

    public void selectFile(TextFile textFile) {
        Logger.info("OSelect file");
        Logger.debug("Select file:  " + (By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, textFile.getName()))));
        Browser.current().waitForAppear(By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, textFile.getName())));
        Browser.current().click(By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, textFile.getName())));
    }

    public void clickDelete() {
        Logger.info("Delete file permanently");
        Browser.current().waitForElementIsClickable(DELETE_BUTTON_LOCATOR);
        Browser.current().click(DELETE_BUTTON_LOCATOR);
        Browser.current().waitForDisappear(By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, FILE_NAME)));
    }

    public void clickGoToDisk() {
        Browser.current().click(YANDEX_DISK_LINK_LOCATOR);
        Browser.current().waitForDisappear(YANDEX_DISK_LINK_LOCATOR);
        Browser.refresh();
    }

    public void clickRestore() {
        Browser.current().waitForElementIsClickable(RESTORE_BUTTON_LOCATOR);
        Browser.current().click(RESTORE_BUTTON_LOCATOR);
        Browser.current().waitForDisappear(By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, FILE_NAME)));
    }
}
