package com.epam.tat2015.home14.tests.disk;

import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.disk.FileBuilder;
import com.epam.tat2015.home14.lib.feature.disk.TextFile;
import com.epam.tat2015.home14.lib.feature.disk.service.DiskLoginService;
import com.epam.tat2015.home14.lib.feature.disk.service.DiskService;
import com.epam.tat2015.home14.lib.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TrashTest {
    private DiskService diskService = new DiskService();

    private FileBuilder fileBuilder = new FileBuilder();
    private DiskLoginService loginService = new DiskLoginService();
    private Account account = AccountBuilder.getDefaultAccount();
    TextFile textFile = fileBuilder.createFile();

    @Test(description = "Check file is moved to trash ")
    public void removeToTrashTest() {
        loginService.loginToDisk(account);
        diskService.uploadFile();
        diskService.removeFile();
        Browser.refresh();
        Assert.assertTrue(diskService.checkFileIsPresentInTrash(textFile), "File is not in trash");
    }

    @Test(description = "Remove file permanently from trash ", dependsOnMethods = "removeToTrashTest")
    public void removePermanentlyTest() {
        Browser.refresh();
        diskService.removeFilePermanently(textFile);
        Assert.assertTrue(diskService.checkFileIsNotPresentInTrash(textFile), "File is in trash");
//        diskService.returnToDisk();
    }
}
