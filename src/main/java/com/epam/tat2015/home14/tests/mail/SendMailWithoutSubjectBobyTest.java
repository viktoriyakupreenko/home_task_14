package com.epam.tat2015.home14.tests.mail;

import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.mail.Letter;
import com.epam.tat2015.home14.lib.feature.mail.LetterBuilder;
import com.epam.tat2015.home14.lib.feature.mail.service.MailLoginService;
import com.epam.tat2015.home14.lib.feature.mail.service.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;


public class SendMailWithoutSubjectBobyTest {

    private MailService mailServise = new MailService();
    private MailLoginService loginService = new MailLoginService();
    private Account account = AccountBuilder.getDefaultAccount();
    Letter letter = LetterBuilder.getLetterWithoutSubjectBody();

    @Test(description = "Send mail without subject and body")
    public void sendMailWithoutSubjectBoby() {
        loginService.loginToMailbox(account);
        mailServise.sendLetterWithoutSubjectBody(letter);
        Assert.assertTrue(mailServise.isLetterSent(), "Mail was not sent");
    }

    @Test(description = "Check that letter without subject is present in inbox  and in sent folder",dependsOnMethods = "sendMailWithoutSubjectBoby")
    public void isMailInInboxInSentTest() {
        Assert.assertTrue(mailServise.isLetterExistWithoutSubject(letter));
    }
}

