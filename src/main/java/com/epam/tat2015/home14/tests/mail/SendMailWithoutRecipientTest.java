package com.epam.tat2015.home14.tests.mail;


import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.mail.Letter;
import com.epam.tat2015.home14.lib.feature.mail.LetterBuilder;
import com.epam.tat2015.home14.lib.feature.mail.service.MailLoginService;
import com.epam.tat2015.home14.lib.feature.mail.service.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.nio.charset.Charset;

public class SendMailWithoutRecipientTest {

    private MailService mailServise = new MailService();
    private MailLoginService loginService = new MailLoginService();
    private Account account = AccountBuilder.getDefaultAccount();
    String EXPECTED_ERROR_MESSAGE = new String("Поле не заполнено. Необходимо ввести адрес.".getBytes(Charset.defaultCharset()), Charset.defaultCharset());

    @Test(description = "Check expected error messages in cases of send mail without properly filled address")
    public void checkErrorMessageSendMailWithoutRecipient() {
        loginService.loginToMailbox(account);
        Letter letter = LetterBuilder.getLetterWithoutRecipient();
        String actualErrorMessage = mailServise.retrieveErrorOnMailWithoutRecipient(letter);
        Assert.assertEquals(actualErrorMessage, EXPECTED_ERROR_MESSAGE, "Error message is not correct");
    }
}
