package com.epam.tat2015.home14.lib.util;

import org.apache.commons.lang3.RandomStringUtils;


public class Randomizer {

    public static final int DEFAULT_LENGTH = 20;

    public static String string() {
        return RandomStringUtils.randomAlphanumeric(DEFAULT_LENGTH);
    }

    public static String numeric() {
        return RandomStringUtils.randomNumeric(DEFAULT_LENGTH);
    }


}
