package com.epam.tat2015.home14.lib.feature.disk;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;
import java.io.IOException;

import static com.epam.tat2015.home14.lib.feature.common.CommonConstants.*;

public class FileBuilder {

    private static String FILE_CONTENT = RandomStringUtils.randomAlphanumeric(20);
    private static TextFile textFile = new TextFile();

    public TextFile createFile() {
        textFile.setName(FILE_NAME);
        textFile.setContent(FILE_CONTENT);
        File file = new File(FILE_PATH_UPLOAD + textFile.getName());
        if (!new File(FILE_PATH_UPLOAD).exists()) {
            file.mkdir();
        }
        try {
            FileUtils.writeStringToFile(file, textFile.getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textFile;
    }

    public TextFile createFiles() {
        TextFile textFile1 = new TextFile();
        TextFile textFile2 = new TextFile();
        TextFile textFile3 = new TextFile();
        textFile1.setName(FILE_NAME_1);
        textFile2.setName(FILE_NAME_2);
        textFile3.setName(FILE_NAME_3);
        textFile1.setContent(FILE_CONTENT);
        textFile2.setContent(FILE_CONTENT);
        textFile3.setContent(FILE_CONTENT);
        File file1 = new File(FILE_PATH_UPLOAD + textFile1.getName());
        File file2 = new File(FILE_PATH_UPLOAD + textFile2.getName());
        File file3 = new File(FILE_PATH_UPLOAD + textFile3.getName());
        if (!new File(FILE_PATH_UPLOAD).exists()) {
            file1.mkdirs();
        }
        if (!new File(FILE_PATH_UPLOAD).exists()) {
            file2.mkdirs();
        }
        if (!new File(FILE_PATH_UPLOAD).exists()) {
            file3.mkdirs();
        }
        try {
            FileUtils.writeStringToFile(file1, textFile1.getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileUtils.writeStringToFile(file2, textFile2.getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            FileUtils.writeStringToFile(file3, textFile3.getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return textFile3;
    }
}
