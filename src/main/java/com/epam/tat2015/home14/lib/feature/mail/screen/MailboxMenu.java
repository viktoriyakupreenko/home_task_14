package com.epam.tat2015.home14.lib.feature.mail.screen;


import com.epam.tat2015.home14.lib.ui.Browser;
import com.epam.tat2015.home14.lib.report.Logger;
import org.openqa.selenium.By;


public class MailboxMenu {

    By SENT_LINK_LOCATOR = By.xpath("//a[@href='#sent']");
    By INBOX_LINK_LOCATOR = By.xpath("//a[@href='#inbox']");
    By DRAFT_LINK_LOCATOR = By.xpath("//a[@href='#draft']");
    By TRASH_LINK_LOCATOR = By.xpath("//a[@href='#trash']");
    By HEAD_TITLE_LOCATOR = By.xpath("//label[@class='b-messages-head__title']");

    public InboxPage openInbox() {
        Logger.info("Open inbox folder");
        Browser.current().click(INBOX_LINK_LOCATOR);
        return new InboxPage();
    }

    public SentPage openSent() {
        Logger.info("Open sent folder");
        Browser.current().click(SENT_LINK_LOCATOR);
        return new SentPage();
    }

    public DeletedPage openDeleted() {
        Logger.info("Open trash folder");
        Browser.current().click(TRASH_LINK_LOCATOR);
        Browser.current().waitForAppear(HEAD_TITLE_LOCATOR);
        return new DeletedPage();
    }

    public DraftsPage openDrafts() {
        Logger.info("Open drafts folder");
        Browser.current().click(DRAFT_LINK_LOCATOR);
        Browser.current().waitForAppear(HEAD_TITLE_LOCATOR);
        return new DraftsPage();
    }

}
