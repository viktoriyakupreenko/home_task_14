package com.epam.tat2015.home14.lib.config;


import com.epam.tat2015.home14.lib.ui.BrowserType;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.spi.StringArrayOptionHandler;
import org.testng.xml.XmlSuite.ParallelMode;

import java.util.List;

public class GlobalConfig {

    private static GlobalConfig instance;

    @Option(name = "-bt", usage = "browser type: firefox_local, chrome_local, firefox_remote or chrome_remote")
    private BrowserType browserType = BrowserType.CHROME;

    @Option(name = "-suites", usage = "list of pathes to suites", handler = StringArrayOptionHandler.class, required = true)
    private List<String> suites = null;

    @Option(name = "-pm", usage = "parallel mode: false or tests")
    private ParallelMode parallelMode = ParallelMode.FALSE;

    @Option(name = "-tc", usage = "amount of threads for parallel execution, equal to 0 by default")
    private int threadCount = 0;

    @Option(name = "-hub", usage = "selenium hub")
    private String seleniumHub = null;

    @Option(name = "-host", usage = "selenium host")
    private String seleniumHost = "http://127.0.0.1";

    @Option(name = "-port", usage = "selenium port")
    private String seleniumPort = "4444";

    @Option(name = "-pathToChromeDriver", usage = "path to chrome driver")
    private String pathToChromeDriver = "./chromedriver_win.exe";

    @Option(name = "-result_dir", usage = "Directory to put results")
    private String resultDir = "results";

    public static GlobalConfig getInstance() {
        if (instance == null) {
            instance = new GlobalConfig();
        }
        return instance;
    }

    public void setBrowserType(BrowserType browserType) {
        this.browserType = browserType;
    }

    public List<String> getSuites() {
        return suites;
    }

    public void setSuites(List<String> suites) {
        this.suites = suites;
    }

    public BrowserType getBrowserType() {
        return browserType;
    }

    public ParallelMode getParallelMode() {
        return parallelMode;
    }

    public void setParallelMode(ParallelMode parallelMode) {
        this.parallelMode = parallelMode;
    }

    public int getThreadCount() {
        return threadCount;
    }

    public void setThreadCount(int threadCount) {
        this.threadCount = threadCount;
    }

    public String getSeleniumHub() {
        return seleniumHub;
    }

    public void setSeleniumHub(String seleniumHub) {
        this.seleniumHub = seleniumHub;
    }

    public String getSeleniumHost() {
        return seleniumHost;
    }

    public String getSeleniumPort() {
        return seleniumPort;
    }

    public String getPathToChromeDriver() {
        return pathToChromeDriver;
    }

    public String getResultDir() {
        return resultDir;
    }


}
