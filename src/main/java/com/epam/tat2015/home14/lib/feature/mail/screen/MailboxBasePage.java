package com.epam.tat2015.home14.lib.feature.mail.screen;

import com.epam.tat2015.home14.lib.ui.Browser;
import org.openqa.selenium.By;


public abstract class MailboxBasePage {

    static By COMPOSE_BUTTON_LOCATOR = By.xpath("//a[@href='#compose']");

    public static MailboxMenu menu() {
        return new MailboxMenu();
    }

    public static WriteLetterPage openWriteLetter() {
        Browser.current().click(COMPOSE_BUTTON_LOCATOR);
        return new WriteLetterPage();
    }

}
