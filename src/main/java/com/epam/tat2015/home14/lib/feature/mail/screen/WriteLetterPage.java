package com.epam.tat2015.home14.lib.feature.mail.screen;


import com.epam.tat2015.home14.lib.ui.Browser;
import com.epam.tat2015.home14.lib.report.Logger;
import org.openqa.selenium.By;


public class WriteLetterPage extends MailboxBasePage {
    By TO_INPUT_LOCATOR = By.xpath("//*[@data-params='field=to']//ancestor::tr//input[@type='text']");
    By SUBJECT_INPUT_LOCATOR = By.id("compose-subj");
    By MAIL_TEXT_LOCATOR = By.id("compose-send");
    By SEND_MAIL_BUTTON_LOCATOR = By.id("compose-submit");
    By ERROR_MESSAGE_LOCATOR = By.xpath("//span[@class='b-notification b-notification_error b-notification_error_required']");
    By SEND_MAIL_MASSAGE_LOCATOR = By.xpath("//div[@class='b-statusline']//a[@class='b-statusline__link']");
    By SAVED_IN_DRAFT_MESSAGE = By.xpath("(//span[@class='b-compose-message__actions__helper b-compose-message__actions__helper_saved'])[2]");

    public void sendMail(String to, String subject, String body) {
        Browser browser = Browser.current();
        Logger.debug("Type - " + to + " into " + TO_INPUT_LOCATOR + "::"
                + "Type - " + subject + " into " + SUBJECT_INPUT_LOCATOR + "::"
                + "Type - " + body + " into " + MAIL_TEXT_LOCATOR + "::");
        browser.writeText(TO_INPUT_LOCATOR, to);
        browser.writeText(SUBJECT_INPUT_LOCATOR, subject);
        browser.writeText(MAIL_TEXT_LOCATOR, body);
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
    }

    public void sendMailWithoutRecipient(String subject, String body) {
        Browser browser = Browser.current();
        Logger.debug("Type - " + subject + " into " + SUBJECT_INPUT_LOCATOR + "::"
                + "Type - " + body + " into " + MAIL_TEXT_LOCATOR + "::");
        browser.writeText(SUBJECT_INPUT_LOCATOR, subject);
        browser.writeText(MAIL_TEXT_LOCATOR, body);
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
    }

    public void sendMailWithoutSubjectBody(String to) {
        Logger.debug("Type - " + to + " into " + TO_INPUT_LOCATOR + "::");
        Browser browser = Browser.current();
        browser.writeText(TO_INPUT_LOCATOR, to);
        browser.click(SEND_MAIL_BUTTON_LOCATOR);
    }

    public void createDraftMail(String to, String subject, String body) {
        Logger.info("Create draft");
        Browser browser = Browser.current();
        Logger.debug("Type - " + to + " into " + TO_INPUT_LOCATOR + "::"
                + "Type - " + subject + " into " + SUBJECT_INPUT_LOCATOR + "::"
                + "Type - " + body + " into " + MAIL_TEXT_LOCATOR + "::");
        browser.writeText(TO_INPUT_LOCATOR, to);
        browser.writeText(SUBJECT_INPUT_LOCATOR, subject);
        browser.writeText(MAIL_TEXT_LOCATOR, body);
    }

    public String getErrorMassage() {
        return Browser.current().getText(ERROR_MESSAGE_LOCATOR);
    }

    public boolean getMassageSavedInDraft() {
        return Browser.current().checkIfElementIsPresent(SAVED_IN_DRAFT_MESSAGE, 20);
    }

    public boolean getConfirmationSentMessage() {
        return Browser.current().checkIfElementIsPresent(SEND_MAIL_MASSAGE_LOCATOR);
    }

    public String[] getSplited_href() {
        Logger.info("Get href");
        String[] splited_href;
        Browser browser = Browser.current();
        String href = browser.getElement(SEND_MAIL_MASSAGE_LOCATOR).getAttribute("href");
        splited_href = href.split("#");
        return splited_href;
    }
}
