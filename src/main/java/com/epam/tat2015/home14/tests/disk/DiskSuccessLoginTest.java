package com.epam.tat2015.home14.tests.disk;

import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.disk.service.DiskLoginService;
import org.testng.annotations.Test;

public class DiskSuccessLoginTest {
    private DiskLoginService loginService = new DiskLoginService();
    private Account account = AccountBuilder.getDefaultAccount();

    @Test(description = "Login to Yandex disk as a valid user")
    public void successLoginToMailbox() {
        loginService.checkSuccessLogin(account);
    }
}
