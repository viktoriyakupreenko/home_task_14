package com.epam.tat2015.home14.lib.feature.disk.service;


import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.disk.screen.DiskBasePage;
import com.epam.tat2015.home14.lib.feature.disk.screen.DiskLoginPage;
import com.epam.tat2015.home14.lib.report.Logger;

public class DiskLoginService {
    public DiskBasePage loginToDisk(Account account) {
        return DiskLoginPage.open().login(account.getLogin(), account.getPassword());
    }

    public void checkSuccessLogin(Account account) {

        Logger.info("Login using credentials " + account.getLogin() + " :: " + account.getPassword());
        String userName = loginToDisk(account).getCurrentLogin();
        if (account == null || !userName.equals(account.getLogin())) {
            throw new RuntimeException("Login to disk failed. Login = " + account.getLogin() + ", Password = " + account.getPassword() + ". Current login = " + userName);
        }
    }

    public String retrieveErrorOnFailedPassword(Account account) {
        loginToDisk(account);
        Logger.info("Retrieve error messages in cases of wrong password");
        return new DiskLoginPage().getErrorMassageWrongPassword();
    }

    public String retrieveErrorOnFailedAccount(Account account) {
        loginToDisk(account);
        Logger.info("Retrieve error messages in cases of wrong account");
        return new DiskLoginPage().getErrorMassageWrongAccount();
    }
}
