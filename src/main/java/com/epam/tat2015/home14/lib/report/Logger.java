package com.epam.tat2015.home14.lib.report;

import org.apache.log4j.Priority;

public class Logger {

    private static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger("com.epam.tat2015.home14");
    
    public static void trace(String message) {
        logger.trace(message);
    }
    
    public static void trace(String message, Throwable t) {
        logger.trace(message, t);
    }
    
    public static void debug(String message, Throwable t) {
        logger.debug(message, t);
    }
    
    public static void error(String message) {
        logger.error(message);
    }
    
    public static void error(String message, Throwable t) {
        logger.error(message, t);
    }
    
    public static void debug(String message) {
        logger.debug(message);
    }

    public static void fatal(String message) {
        logger.fatal(message);
    }

    public static void fatal(String message, Throwable t) {
        logger.fatal(message, t);
    }

    public static void info(String message) {
        logger.info(message);
    }

    public static void info(String message, Throwable t) {
        logger.info(message, t);
    }

    public static void save(String pathToFile) {
        Logger.log(LoggerLevel.SAVE, pathToFile);
    }

    public static void log(Priority priority, String message, Throwable t) {
        logger.log(priority, message, t);
    }

    public static void log(Priority priority, String message) {
        logger.log(priority, message);
    }
}
