package com.epam.tat2015.home14.lib.feature.mail.screen;


import com.epam.tat2015.home14.lib.feature.mail.Letter;
import com.epam.tat2015.home14.lib.ui.Browser;
import com.epam.tat2015.home14.lib.report.Logger;
import org.openqa.selenium.By;

public class DraftsPage extends MailboxBasePage {
    private static String checkBox = "//*[@class='block-messages']//a[contains(., '%s')]/ancestor::span/preceding-sibling::label/input";
    By DELETE_BUTTON_LOCATOR = By.xpath(" //a[@data-action='delete']");

    public void clickCheckbox(Letter letter) {
        Logger.info("Click check box");
        while (Browser.current().checkIfElementIsNotPresent(By.xpath(String.format(checkBox, letter.getSubject())))) {
            Browser.current().refresh();
        }
        Browser.current().click(By.xpath(String.format(checkBox, letter.getSubject())));
    }

    public void clickDelete() {
        Logger.info("Click delete button");
        Browser.current().click(DELETE_BUTTON_LOCATOR);
    }
}

