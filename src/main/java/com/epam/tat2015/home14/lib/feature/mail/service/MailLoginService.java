package com.epam.tat2015.home14.lib.feature.mail.service;

import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.mail.screen.InboxPage;
import com.epam.tat2015.home14.lib.feature.mail.screen.LoginFailedPage;
import com.epam.tat2015.home14.lib.feature.mail.screen.MailLoginPage;
import com.epam.tat2015.home14.lib.report.Logger;

public class MailLoginService {

    public InboxPage loginToMailbox(Account account) {
        return MailLoginPage.open().login(account.getLogin(), account.getPassword());
    }

    public void checkSuccessLogin(Account account) {
        Logger.info("Login using credentials " + account.getLogin() + " :: " + account.getPassword());
        String address = loginToMailbox(account).getCurrentAddress();
        if (account == null || !address.equals(account.getEmail())) {
            throw new RuntimeException("Login to mailbox failed. Login = " + account.getLogin() + ", Password = " + account.getPassword() + ". Current email = " + address);
        }
    }

    public String retrieveErrorOnFailedLogin(Account account) {
        loginToMailbox(account);
        Logger.info("Retrieve error messages ");
        return new LoginFailedPage().getErrorMassage();
    }

}
