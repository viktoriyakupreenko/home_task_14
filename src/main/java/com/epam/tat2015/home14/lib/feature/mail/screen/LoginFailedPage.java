package com.epam.tat2015.home14.lib.feature.mail.screen;

import com.epam.tat2015.home14.lib.ui.Browser;
import com.epam.tat2015.home14.lib.report.Logger;
import org.openqa.selenium.By;

public class LoginFailedPage {

    By ERROR_MESSAGE_LOCATOR = By.className("error-msg");

    public String getErrorMassage() {
        Logger.debug("Error messages :  " + ERROR_MESSAGE_LOCATOR);
        return Browser.current().getText(ERROR_MESSAGE_LOCATOR);
    }
}
