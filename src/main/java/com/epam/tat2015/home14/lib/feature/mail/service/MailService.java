package com.epam.tat2015.home14.lib.feature.mail.service;

import com.epam.tat2015.home14.lib.feature.mail.Letter;
import com.epam.tat2015.home14.lib.feature.mail.screen.*;
import com.epam.tat2015.home14.lib.ui.Browser;
import com.epam.tat2015.home14.lib.report.Logger;
import org.openqa.selenium.By;

import static com.epam.tat2015.home14.lib.feature.common.CommonConstants.*;

public class MailService {

    public void sendLetter(Letter letter) {
        Logger.info("Send letter");
        WriteLetterPage.openWriteLetter()
                .sendMail(letter.getRecipient(), letter.getSubject(), letter.getBody());
    }

    public void sendLetterWithoutSubjectBody(Letter letter) {
        Logger.info("Send letter without subject");
        WriteLetterPage.openWriteLetter()
                .sendMailWithoutSubjectBody(letter.getRecipient());
    }

    public void createDraftMail(Letter letter) {
        Logger.info("Create draft");
        WriteLetterPage.openWriteLetter()
                .createDraftMail(letter.getRecipient(), letter.getSubject(), letter.getBody());
    }

    public boolean isLetterExistInInbox(Letter letter) {
        MailboxBasePage.menu().openInbox();
        Logger.debug("Check file is exist in inbox : " + " Letter with : recipient = '" + letter.getRecipient() + "'" + " subject = '" + letter.getSubject() + "'" + " body = '" + letter.getBody() + "'"); // Logger
        return Browser.current().checkIfElementIsPresent(By.xpath(String.format(LETTER_LOCATOR_PATTERN, letter.getSubject())));
    }

    public boolean isLetterExistInSent(Letter letter) {
        MailboxBasePage.menu().openSent();
        Logger.debug("Check is letter exist in Sent : " + " Letter with : recipient = '" + letter.getRecipient() + "'" + " subject = '" + letter.getSubject() + "'" + " body = '" + letter.getBody() + "'"); // Logger
        return Browser.current().checkIfElementIsPresent(By.xpath(String.format(LETTER_LOCATOR_PATTERN, letter.getSubject())));
    }

    public boolean LetterIsNotInDraft(Letter letter) {
        Logger.debug("Check was deleted exist in Draft : " + " Letter with : recipient = '" + letter.getRecipient() + "'" + " subject = '" + letter.getSubject() + "'" + " body = '" + letter.getBody() + "'"); // Logger
        return Browser.current().checkIfElementIsNotPresent(By.xpath(String.format(LETTER_LOCATOR_PATTERN, letter.getSubject())));
    }

    public boolean LetterIsNotInTrash(Letter letter) {
        Logger.debug("Check was deleted exist in trash : " + " Letter with : recipient = '" + letter.getRecipient() + "'" + " subject = '" + letter.getSubject() + "'" + " body = '" + letter.getBody() + "'"); // Logger
        return Browser.current().checkIfElementIsNotPresent(By.xpath(String.format(LETTER_LOCATOR_PATTERN, letter.getSubject())));
    }

    public boolean isLetterExistWithoutSubject(Letter letter) {
        String[] splited_href = new WriteLetterPage().getSplited_href();
        MailboxBasePage.menu().openInbox();
        Logger.debug("Check is letter exist in Inbox : " + " Letter with : recipient = '" + letter.getRecipient() + "'" + " id = '" + splited_href[splited_href.length - 1] + "'"); // Logger
        Browser.current().checkIfElementIsPresent(By.xpath(String.format(MAIL_ID_LOCATOR_PATTERN, splited_href[splited_href.length - 1])));
        MailboxBasePage.menu().openSent();
        Logger.debug("Check is letter exist in Sent : " + " Letter with : recipient = '" + letter.getRecipient() + "'" + " id = '" + splited_href[splited_href.length - 1] + "'"); // Logger
        Browser.current().checkIfElementIsPresent(By.xpath(String.format(MAIL_ID_LOCATOR_PATTERN, splited_href[splited_href.length - 1])));
        return true;
    }

    public String retrieveErrorOnMailWithoutRecipient(Letter letter) {
        Logger.info("Retrieve error on mail without subject");
        WriteLetterPage.openWriteLetter()
                .sendMailWithoutRecipient(letter.getSubject(), letter.getBody());
        String errorMessage = new WriteLetterPage().getErrorMassage();
        Logger.debug("Check is error message: " + errorMessage); // Logger
        return errorMessage;
    }

    public boolean isLetterSent() {
        return new WriteLetterPage().getConfirmationSentMessage();
    }

    public boolean savedInDraft() {
        Logger.info("Check confirmation message saved in draft");
        return new WriteLetterPage().getMassageSavedInDraft();
    }

    public void deleteMailDraft(Letter letter) {
        Logger.info("Delete draft mail");
        DraftsPage draftsPage = MailboxBasePage.menu().openDrafts();
        draftsPage.clickCheckbox(letter);
        draftsPage.clickDelete();
    }

    public void deleteMailTrash(Letter letter) {
        Logger.info("Delete mailt from trash");
        DeletedPage deletedPage = MailboxBasePage.menu().openDeleted();
        deletedPage.clickCheckbox(letter);
        deletedPage.clickDelete();
    }
}
