package com.epam.tat2015.home14.tests.mail;

import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.mail.service.MailLoginService;
import org.testng.annotations.Test;


public class MailSuccessLoginTest {

    private MailLoginService loginService = new MailLoginService();
    private Account account = AccountBuilder.getDefaultAccount();

    @Test(description = "Login to mailbox as a valid user")
    public void successLoginToMailbox() {
        loginService.checkSuccessLogin(account);
    }

}
