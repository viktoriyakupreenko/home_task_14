package com.epam.tat2015.home14.lib.feature.mail.screen;

import com.epam.tat2015.home14.lib.ui.Browser;
import org.openqa.selenium.By;


public class InboxPage extends MailboxBasePage {
    By CURRENT_ADDRESSS_ELEMENT = By.cssSelector("#nb-1");

    public String getCurrentAddress() {
        Browser.current().waitForAppear(CURRENT_ADDRESSS_ELEMENT);
        return Browser.current().getText(CURRENT_ADDRESSS_ELEMENT).trim();

    }
}
