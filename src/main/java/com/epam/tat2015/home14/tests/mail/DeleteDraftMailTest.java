package com.epam.tat2015.home14.tests.mail;


import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.mail.Letter;
import com.epam.tat2015.home14.lib.feature.mail.LetterBuilder;
import com.epam.tat2015.home14.lib.feature.mail.service.MailLoginService;
import com.epam.tat2015.home14.lib.feature.mail.service.MailService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DeleteDraftMailTest {
    private MailService mailServise = new MailService();
    private MailLoginService loginService = new MailLoginService();
    private Account account = AccountBuilder.getDefaultAccount();
    Letter letter = LetterBuilder.getDefaultLetter();

    @Test(description = "Check that draft mail was created")
    public void createDraftMail() {
        loginService.loginToMailbox(account);
        mailServise.createDraftMail(letter);
        Assert.assertTrue(mailServise.savedInDraft(), "Mail was not saved in Draft");
    }

    @Test(description = "Delete mail from drafts", dependsOnMethods = "createDraftMail")
    public void deleteMailFromDraft() {
        mailServise.deleteMailDraft(letter);
        Assert.assertTrue(mailServise.LetterIsNotInDraft(letter), "Mail is in Draft folder");
    }

    @Test(description = "Delete mail from trash", dependsOnMethods =  "deleteMailFromDraft")
    public void deleteMailFromTrash() {
        mailServise.deleteMailTrash(letter);
        Assert.assertTrue(mailServise.LetterIsNotInTrash(letter), "Mail is in trash folder");
    }
}
