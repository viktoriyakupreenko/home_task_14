package com.epam.tat2015.home14.tests.disk;

import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.disk.service.DiskLoginService;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.nio.charset.Charset;


public class DiskMissedCredentialLoginTest {

    private DiskLoginService loginService = new DiskLoginService();

    @Test(description = "Check expected error messages in cases of wrong password ")
    public void checkErrorMessageWithWrongPassword() {
        String EXPECTED_ERROR_1;
        EXPECTED_ERROR_1 = new String("Неправильная пара логин-пароль".getBytes(Charset.defaultCharset()), Charset.defaultCharset());
        Account account1 = AccountBuilder.getAccountWithWrongPass();
        String actualErrorMessage = loginService.retrieveErrorOnFailedPassword(account1);
        Assert.assertEquals(actualErrorMessage, EXPECTED_ERROR_1, "Error message is not correct");
    }

    @Test(description = "Check expected error messages in cases of non existed account")
    public void checkErrorMessageWithWrongAccount() {
        String EXPECTED_ERROR_2 = new String("Учётной записи с таким логином не существует".getBytes(Charset.defaultCharset()), Charset.defaultCharset());
        Account account2 = AccountBuilder.getNonExistedAccount();
        String actualErrorMessage = loginService.retrieveErrorOnFailedAccount(account2);
        Assert.assertEquals(actualErrorMessage, EXPECTED_ERROR_2, "Error message is not correct");
    }

}

