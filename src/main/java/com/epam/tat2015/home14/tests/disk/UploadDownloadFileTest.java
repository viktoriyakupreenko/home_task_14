package com.epam.tat2015.home14.tests.disk;

import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.disk.FileBuilder;
import com.epam.tat2015.home14.lib.feature.disk.TextFile;
import com.epam.tat2015.home14.lib.feature.disk.service.DiskLoginService;
import com.epam.tat2015.home14.lib.feature.disk.service.DiskService;
import com.epam.tat2015.home14.lib.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UploadDownloadFileTest {
    private DiskService diskService = new DiskService();
    private FileBuilder fileBuilder = new FileBuilder();
    private DiskLoginService loginService = new DiskLoginService();
    private Account account = AccountBuilder.getDefaultAccount();
    TextFile textFile = fileBuilder.createFile();

    @Test(description = "Check that correct file was uploaded ")
    public void uploadFileTest() {
        loginService.loginToDisk(account);
        diskService.uploadFile();
        Assert.assertTrue(diskService.checkFieIsPresent(textFile), "File was not uploaded");
    }

    @Test(description = "Check file is downloaded and has expected content ", dependsOnMethods = "uploadFileTest")
    public void downloadFileTest() {
        Browser.refresh();
        diskService.downloadFile(textFile);
        Assert.assertEquals(diskService.checkContent(), textFile.getContent(), "File was not downloaded");
    }
}
