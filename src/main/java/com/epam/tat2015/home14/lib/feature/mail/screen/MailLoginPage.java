package com.epam.tat2015.home14.lib.feature.mail.screen;

import com.epam.tat2015.home14.lib.ui.Browser;
import org.openqa.selenium.By;

import static com.epam.tat2015.home14.lib.feature.common.CommonConstants.MAILBOX_URL;

public class MailLoginPage {

    By LOGIN_INPUT = By.name("login");
    By PASS_INPUT = By.name("passwd");
    By SUBMIT_BUTTON = By.cssSelector("button[type='submit']");

    public InboxPage login(String login, String pass) {
        Browser browser = Browser.current();
        browser.writeText(LOGIN_INPUT, login);
        browser.writeText(PASS_INPUT, pass);
        browser.click(SUBMIT_BUTTON);
        return new InboxPage();
    }

    public static MailLoginPage open() {
        Browser browser = Browser.rise();
        browser.open(MAILBOX_URL);
        MailLoginPage mailLoginPage = new MailLoginPage();
        Browser.current().waitForAppear(mailLoginPage.LOGIN_INPUT);
        return mailLoginPage;
    }

}
