package com.epam.tat2015.home14.lib.ui;

import com.epam.tat2015.home14.lib.config.GlobalConfig;
import com.epam.tat2015.home14.lib.report.Logger;
import org.apache.commons.io.FileUtils;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.internal.WrapsDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.epam.tat2015.home14.lib.feature.common.CommonConstants.FILE_PATH_DOWNLOAD;


public class Browser implements WrapsDriver {


    public static final int COMMON_ELEMENT_WAIT_TIME_OUT = 20;
    public static final int COMMON_ELEMENT_SHORT_TIME_OUT = 5;
    public static final long WEBDRIVER_IMPLICIT_TIME_OUT = 5;
    private WebDriver driver;

    private static Map<Thread, Browser> instances = new HashMap<>();

    public static synchronized Browser rise() {
        if (instances.get(Thread.currentThread()) != null) {
            closeCurrentBrowser();
        }

        Browser browser = new Browser();
        browser.createDriver();
        instances.put(Thread.currentThread(), browser);
        return browser;
    }

    public static synchronized Browser current() {
        if (instances.get(Thread.currentThread()) == null) {
            rise();
        }
        return instances.get(Thread.currentThread());
    }


    public static synchronized void closeCurrentBrowser() {
        Browser browser = instances.get(Thread.currentThread());
        if (browser == null) {
            return;
        }

        instances.remove(Thread.currentThread());
        browser.quit();
    }

    private static Browser browserFotThread() {
        return instances.get(Thread.currentThread());
    }
    private synchronized static boolean isBrowserOpened() {
        Browser browser = browserFotThread();
        return browser != null && browser.getWrappedDriver() != null;
    }

    public void quit() {
        Logger.debug("Stop browser");
        try {
            WebDriver wrappedDriver = getWrappedDriver();
            if (wrappedDriver != null) {
                wrappedDriver.quit();
            }
        } catch (Exception ignore) {
        } finally {
            instances.remove(Thread.currentThread());
        }
    }

    private void createDriver() {
        switch (GlobalConfig.getInstance().getBrowserType()) {
            case FIREFOX:
                Logger.debug("Start firefox browser");
                this.driver = isRemote() ? remoteFirefoxDriver() : localFirefoxDriver();
                break;
            case CHROME:
                Logger.debug("Start chrome browser");
                this.driver = isRemote() ? remoteChromeDriver() : localChromeDriver();
                break;
            default:
        }
        driver.manage().window().maximize();
//        driver.manage().timeouts().pageLoadTimeout(WEBDRIVER_IMPLICIT_TIME_OUT, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(WEBDRIVER_IMPLICIT_TIME_OUT, TimeUnit.SECONDS);
    }

    private boolean isRemote() {
        String seleniumHub = GlobalConfig.getInstance().getSeleniumHub();
        return seleniumHub != null && seleniumHub != "";
    }

    private FirefoxProfile settingsFirefox() {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setAlwaysLoadNoFocusLib(true);
        profile.setEnableNativeEvents(false);
        profile.setAssumeUntrustedCertificateIssuer(true);
        profile.setAcceptUntrustedCertificates(true);
        profile.setPreference("browser.download.folderList", 2);
        profile.setPreference("browser.download.dir", FILE_PATH_DOWNLOAD);
        profile.setPreference("browser.helperApps.neverAsk.openFile", "text/plain,application/octet-stream");
        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "text/plain,application/octet-stream");
        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
        profile.setPreference("browser.download.manager.showWhenStarting", false);
        return profile;
    }

    private DesiredCapabilities settingsChrome() {
        System.setProperty("webdriver.chrome.driver", GlobalConfig.getInstance().getPathToChromeDriver());
//        System.setProperty("webdriver.chrome.driver", "./src/main/resources/chromedriver_win.exe");
        HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
        chromePrefs.put("profile.default_content_settings.popups", 0);
        chromePrefs.put("download.default_directory", FILE_PATH_DOWNLOAD);
        ChromeOptions options = new ChromeOptions();
        options.setExperimentalOption("prefs", chromePrefs);
        options.addArguments("--disable-web-security");
        options.addArguments("--allow-running-insecure-content");
        DesiredCapabilities cap = DesiredCapabilities.chrome();
        cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
        cap.setCapability(ChromeOptions.CAPABILITY, options);
        return cap;
    }

    private WebDriver localFirefoxDriver() {
        return new FirefoxDriver(settingsFirefox());
    }

    private WebDriver localChromeDriver() {
        return new ChromeDriver(settingsChrome());
    }

    private WebDriver remoteFirefoxDriver() {
        DesiredCapabilities capabilities = DesiredCapabilities.firefox();
        capabilities.setCapability(FirefoxDriver.PROFILE, settingsFirefox());
        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL(GlobalConfig.getInstance().getSeleniumHub()), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return driver;
    }

    private WebDriver remoteChromeDriver() {
        WebDriver driver = null;
        try {
            driver = new RemoteWebDriver(new URL(GlobalConfig.getInstance().getSeleniumHub()), settingsChrome());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return driver;
    }

    public WebDriver getWrappedDriver() {
        return driver;
    }

    public void open(String url) {
        Logger.debug("Open page : " + url); // Logger
        getWrappedDriver().get(url);
        screenshot();
    }

    public void waitForAppear(By locator) {
        Logger.debug("Wait for element appear : " + locator); // Logger
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_SHORT_TIME_OUT).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void waitForAppear(By locator, int time) {
        Logger.debug("Wait for element appear : " + locator); // Logger
        new WebDriverWait(getWrappedDriver(), time).until(ExpectedConditions.presenceOfElementLocated(locator));
    }

    public void waitForDisappear(By locator) {
        Logger.debug("Wait for element disappear : " + locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_SHORT_TIME_OUT).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public void waitForDisappear(By locator, int time) {
        Logger.debug("Wait for element disappear : " + locator);
        new WebDriverWait(getWrappedDriver(), time).until(ExpectedConditions.invisibilityOfElementLocated(locator));
    }

    public WebElement waitForElementIsClickable(By locator) {
        Logger.debug("Wait for element clickable : " + locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.elementToBeClickable(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForElementIsVisible(By locator) {
        Logger.debug("Wait for element visible : " + locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.visibilityOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public WebElement waitForElementIsPresent(By locator) {
        Logger.debug("Wait for element is present : " + locator);
        new WebDriverWait(getWrappedDriver(), COMMON_ELEMENT_WAIT_TIME_OUT).until(ExpectedConditions.presenceOfElementLocated(locator));
        return driver.findElement(locator);
    }

    public void writeText(By locator, String text) {
        Logger.debug("Write text to : " + locator + " text = '" + text + "'"); // Logger
        waitForElementIsVisible(locator);
        getWrappedDriver().findElement(locator).sendKeys(text);
        screenshot();
    }

    public void click(By locator) {
        Logger.debug("Click element : " + locator);// Logger
        screenshot();
        try {
            waitForAppear(locator);
            waitForElementIsClickable(locator);
            getWrappedDriver().findElement(locator).click();
        } catch (Exception e) {
            clickJS(locator);
        }
        screenshot();
    }

    public void clickJS(By locator) {
        JavascriptExecutor executor = (JavascriptExecutor) getWrappedDriver();
        executor.executeScript("arguments[0].click();", getElement(locator));
        screenshot();
    }

    public String getText(By locator) {
        Logger.debug("Get text of element : " + locator); // Logger
        screenshot();
        return getWrappedDriver().findElement(locator).getText();
    }

    public WebElement getElement(By locator) {
        Logger.debug("Get element : " + locator); // Logger
        waitForElementIsPresent(locator);
        return getWrappedDriver().findElement(locator);
    }

    public static void screenshot() {
        WebDriver driver = current().getWrappedDriver();
        if (isBrowserOpened()) {
            try {
                byte[] screenshotBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
                File screenshotFile = new File(screenshotName());
                FileUtils.writeByteArrayToFile(screenshotFile, screenshotBytes);
                Logger.save(screenshotFile.getName());
            } catch (Exception e) {
                Logger.error("Failed to write screenshot: " + e.getMessage(), e);
            }
        }
    }

    private static String screenshotName() {
        return GlobalConfig.getInstance().getResultDir() + File.separator + System.nanoTime() + ".png";
    }

    public boolean checkIfElementIsPresent(By locator) {
        Logger.info("Check element is present: " + locator);
        try {
            waitForAppear(locator);
        } catch (Exception e) {
            return false;
        }
        screenshot();
        return true;
    }

    public boolean checkIfElementIsPresent(By locator, int time) {
        Logger.info("Check element is present: " + locator);
        try {
            waitForAppear(locator, time);
        } catch (Exception e) {
            return false;
        }
        screenshot();
        return true;
    }

    public boolean checkIfElementIsNotPresent(By locator) {
        Logger.info("Check element is not present: " + locator);
        try {
            waitForDisappear(locator);
        } catch (Exception e) {
            return false;
        }
        screenshot();
        return true;
    }

    public static void refresh() {
        Logger.info("Refresh page");
        instances.get(Thread.currentThread()).getWrappedDriver().navigate().refresh();
        screenshot();
    }

}
