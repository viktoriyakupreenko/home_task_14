package com.epam.tat2015.home14.lib.feature.disk.service;

import com.epam.tat2015.home14.lib.feature.disk.TextFile;
import com.epam.tat2015.home14.lib.feature.disk.screen.DiskBasePage;
import com.epam.tat2015.home14.lib.feature.disk.screen.DiskTrashPage;
import com.epam.tat2015.home14.lib.ui.Browser;
import com.epam.tat2015.home14.lib.report.Logger;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import java.io.File;
import java.io.IOException;

import static com.epam.tat2015.home14.lib.feature.common.CommonConstants.*;


public class DiskService {
    private DiskBasePage diskBasePage = new DiskBasePage();
    private DiskTrashPage diskTrashPage = new DiskTrashPage();

    public void uploadFile() {
        Logger.info("Upload file");
        diskBasePage.uploadFile();
        diskBasePage.closeUploadWindow();
    }

    public void uploadSeveralFiles() {
        Logger.info("Upload several  files");
        diskBasePage.uploadSeveralFiles();
        diskBasePage.closeUploadWindow();

    }

    public void downloadFile(TextFile textFile) {
        Logger.info("Download file");
        diskBasePage.selectFile(textFile);
        diskBasePage.clickDownloadFile();
    }

    public boolean checkFieIsPresent(TextFile textFile) {
        Logger.info("Check file is present");
        Browser.current().waitForAppear(By.xpath(String.format(FILE_LOCATOR_PATTERN, textFile.getName())));
        return Browser.current().checkIfElementIsPresent(By.xpath(String.format(FILE_LOCATOR_PATTERN, textFile.getName())));
    }

    public boolean checkFileIsPresentInTrash(TextFile textFile) {
        Logger.info("Check file is present in trash");
        diskTrashPage.openTrash();
        Browser.current().waitForElementIsClickable(By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, textFile.getName())));
        return Browser.current().checkIfElementIsPresent(By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, textFile.getName())));
    }

    public boolean checkFileIsNotPresentInTrash(TextFile textFile) {
        Logger.info("Check file is not present");
        return Browser.current().checkIfElementIsNotPresent(By.xpath(String.format(TRASH_FILE_LOCATOR_PATTERN, textFile.getName())));
    }

    public String checkContent() {
        Logger.info("Check file content:  " + FILE_PATH_DOWNLOAD + FILE_NAME);
        File file = new File(FILE_PATH_DOWNLOAD + FILE_NAME);

        while (!file.exists()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        String actualContent = null;
        try {
            actualContent = FileUtils.readFileToString(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Logger.info("Actual content of downloaded file: " + actualContent);
        return actualContent;

    }

    public void removeFile() {
        Logger.info("Remove file to trash");
        Browser.refresh();
        WebElement file = DiskBasePage.getFileElement();
        WebElement trashBox = diskBasePage.getTrashElement();
        new Actions(Browser.current().getWrappedDriver()).dragAndDrop(file, trashBox).build().perform();
    }

    public void removeFiles() {
        Logger.info("Remove several files to trash");
        Browser.refresh();
        DiskBasePage.selectSeveralElements();
        WebElement file = DiskBasePage.getFileElements();
        WebElement trashBox = diskBasePage.getTrashElement();
        new Actions(Browser.current().getWrappedDriver()).dragAndDrop(file, trashBox).build().perform();
    }

    public void removeFilePermanently(TextFile textFile) {
        Logger.info("Remove file permanently");
        Browser.refresh();
        diskTrashPage.selectFile(textFile);
        diskTrashPage.clickDelete();
    }

    public void returnToDisk() {
        Logger.info("Return to base page Yandex dick");
        diskTrashPage.clickGoToDisk();
    }

    public void restoreFile(TextFile textFile) {
        Logger.info("Restore file");
        diskTrashPage.openTrash();
        Browser.refresh();
        diskTrashPage.selectFile(textFile);
        diskTrashPage.clickRestore();
    }

}
