package com.epam.tat2015.home14.lib.feature.common;


import com.epam.tat2015.home14.lib.util.Randomizer;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.File;

public interface CommonConstants {
    String MAILBOX_URL = "http://mail.yandex.ru";
    String DISK_URL = "http://disk.yandex.ru";
    String DEFAULT_USER_LOGIN = "tat-test-user";
    String DEFAULT_USER_PASSWORD = "tat-123qwe";
    String DEFAULT_USER_MAIL = "tat-test-user@yandex.ru";
    String emailSubject = RandomStringUtils.randomAlphanumeric(20);
    String emailBodyText = RandomStringUtils.randomAlphanumeric(10);
    String LETTER_LOCATOR_PATTERN = "//*[text()='%s']";
    String MAIL_ID_LOCATOR_PATTERN = "//a[@class='b-messages__message__link daria-action' and @href='#%s']";
    String FILE_PATH_DOWNLOAD = System.getProperty("user.dir") + File.separator + "downloads" + File.separator;
    String FILE_PATH_UPLOAD = System.getProperty("user.dir") + File.separator + "uploads" + File.separator;
    String FILE_NAME = "test" + Randomizer.numeric() + ".txt";
    String FILE_NAME_1 = "test" + Randomizer.numeric() + ".txt";
    String FILE_NAME_2 = "test" + Randomizer.numeric() + ".txt";
    String FILE_NAME_3 = "test" + Randomizer.numeric() + ".txt";
    String FILE_LOCATOR_PATTERN = " //div[@data-id='/disk/%s']";
    String TRASH_FILE_LOCATOR_PATTERN = " //div[@data-id='/trash/%s']";
}
