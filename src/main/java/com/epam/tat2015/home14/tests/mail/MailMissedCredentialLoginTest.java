package com.epam.tat2015.home14.tests.mail;

import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.mail.service.MailLoginService;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.nio.charset.Charset;


public class MailMissedCredentialLoginTest {


    private MailLoginService loginService = new MailLoginService();

    @Test(description = "Check expected error messages in cases of wrong password and of non existed account", dataProvider = "dataProviderForMailMissedCredentialLoginTest")
    public void checkErrorMessageMissedCredentialLogin(Account account, String EXPECTED_ERROR, String testName) {
        System.out.println(testName);
        String actualErrorMessage = loginService.retrieveErrorOnFailedLogin(account);
        Assert.assertEquals(actualErrorMessage, EXPECTED_ERROR, "Error message is not correct");
    }

    @DataProvider(name = "dataProviderForMailMissedCredentialLoginTest")
    public Object[][] dataProvider() {
        Account account1 = AccountBuilder.getAccountWithWrongPass();
        Account account2 = AccountBuilder.getNonExistedAccount();
        String EXPECTED_ERROR_1 = new String("Неправильный логин или пароль.".getBytes(Charset.defaultCharset()), Charset.defaultCharset());
        String EXPECTED_ERROR_2 = new String("Нет аккаунта с таким логином.".getBytes(Charset.defaultCharset()), Charset.defaultCharset());
        String testName1 = "Check expected error message in cases of wrong password";
        String testName2 = "Check expected error message in case of non existed account";

        return new Object[][]{
                {account1, EXPECTED_ERROR_1, testName1},
                {account2, EXPECTED_ERROR_2, testName2}
        };
    }
}
