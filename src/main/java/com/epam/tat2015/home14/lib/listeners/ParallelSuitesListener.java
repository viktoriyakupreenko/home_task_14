package com.epam.tat2015.home14.lib.listeners;

import com.epam.tat2015.home14.lib.config.GlobalConfig;
import org.testng.ISuite;
import org.testng.ISuiteListener;

public class ParallelSuitesListener implements ISuiteListener {
    @Override
    public void onStart(ISuite suite) {
        suite.getXmlSuite().setParallel(GlobalConfig.getInstance().getParallelMode());
        suite.getXmlSuite().setThreadCount(GlobalConfig.getInstance().getThreadCount());
    }

    @Override
    public void onFinish(ISuite suite) {

    }
}
