package com.epam.tat2015.home14.lib.feature.disk.screen;

import com.epam.tat2015.home14.lib.ui.Browser;
import com.epam.tat2015.home14.lib.report.Logger;
import org.openqa.selenium.By;

import static com.epam.tat2015.home14.lib.feature.common.CommonConstants.DISK_URL;

public class DiskLoginPage {

    By LOGIN_INPUT = By.name("login");
    By PASS_INPUT = By.name("password");
    By SUBMIT_BUTTON = By.cssSelector("button[type='submit']");
    By ERROR_MESSAGE_WRONG_PASSWORD = By.id("nb-input_error1");
    By ERROR_MESSAGE_WRONG_ACCOUNT = By.id("nb-input_error0");

    public DiskBasePage login(String login, String pass) {
        Logger.info("Login into Yandex disk  page");
        Browser browser = Browser.current();
        Logger.debug("Type " + login + " into " + LOGIN_INPUT);
        browser.writeText(LOGIN_INPUT, login);
        Logger.debug("Type " + pass + " into " + PASS_INPUT);
        browser.writeText(PASS_INPUT, pass);
        browser.click(SUBMIT_BUTTON);
        return new DiskBasePage();
    }

    public static DiskLoginPage open() {
        Logger.info("Open Yandex disk login page");
        Browser browser = Browser.rise();
        browser.open(DISK_URL);
        DiskLoginPage diskLoginPage = new DiskLoginPage();
        Browser.current().waitForAppear(diskLoginPage.LOGIN_INPUT);
        return diskLoginPage;
    }

    public String getErrorMassageWrongPassword() {
        Logger.debug("Error messages in cases of wrong password:  " + ERROR_MESSAGE_WRONG_PASSWORD);
        return Browser.current().getText(ERROR_MESSAGE_WRONG_PASSWORD);

    }

    public String getErrorMassageWrongAccount() {
        Logger.debug("Error messages in cases of wrong account:  " + ERROR_MESSAGE_WRONG_ACCOUNT);
        return Browser.current().getText(ERROR_MESSAGE_WRONG_ACCOUNT);
    }

}
