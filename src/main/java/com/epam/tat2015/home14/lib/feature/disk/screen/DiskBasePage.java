package com.epam.tat2015.home14.lib.feature.disk.screen;


import com.epam.tat2015.home14.lib.feature.disk.TextFile;
import com.epam.tat2015.home14.lib.ui.Browser;
import com.epam.tat2015.home14.lib.report.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import static com.epam.tat2015.home14.lib.feature.common.CommonConstants.*;

public class DiskBasePage {
    By UPLOAD_BUTTON_LOCATOR = By.xpath("//input[@class='button__attach']");
    By DOWNLOAD_BUTTON_LOCATOR = By.xpath("//button[@data-click-action='resource.download']");
    By TRASH_BOX_LOCATOR = By.xpath(" //div[@data-id='/trash']/div");
    By CURRENT_ADDRESS_ELEMENT = By.xpath("//div[@class='header__user i-bem _popup-destructor_js_inited']/span");

    public void uploadFile() {
        Logger.debug("Get element:    '" + UPLOAD_BUTTON_LOCATOR + "'    " + "send key '" + FILE_PATH_UPLOAD + FILE_NAME + "'");
        Browser.current().getElement(UPLOAD_BUTTON_LOCATOR).sendKeys(FILE_PATH_UPLOAD + FILE_NAME);
        Browser.current().waitForAppear(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME, 10)));
    }

    public void uploadSeveralFiles() {
        Browser.current().getElement(UPLOAD_BUTTON_LOCATOR).sendKeys(FILE_PATH_UPLOAD + FILE_NAME_1);
        Browser.current().getElement(UPLOAD_BUTTON_LOCATOR).sendKeys(FILE_PATH_UPLOAD + FILE_NAME_2);
        Browser.current().getElement(UPLOAD_BUTTON_LOCATOR).sendKeys(FILE_PATH_UPLOAD + FILE_NAME_3);
        Browser.current().waitForAppear(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME_3, 10)));
    }

    public void closeUploadWindow() {
        Browser.refresh();
    }

    public void selectFile(TextFile textFile) {
        Logger.info("Select file:   '" + By.xpath(String.format(FILE_LOCATOR_PATTERN, textFile.getName())) + "'");
        Browser.current().click(By.xpath(String.format(FILE_LOCATOR_PATTERN, textFile.getName())));
    }

    public static void selectSeveralElements() {
        Browser.refresh();
        WebElement file = Browser.current().getElement(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME_1)));
        WebElement file1 = Browser.current().getElement(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME_2)));
        WebElement file2 = Browser.current().getElement(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME_3)));
        Actions actions = new Actions(Browser.current().getWrappedDriver());
        actions.keyDown(Keys.CONTROL)
                .click(file)
                .click(file1)
                .click(file2)
                .keyUp(Keys.CONTROL)
                .build()
                .perform();
    }

    public void clickDownloadFile() {
        Browser.current().waitForElementIsClickable(DOWNLOAD_BUTTON_LOCATOR);
        Browser.current().getElement(DOWNLOAD_BUTTON_LOCATOR).click();
    }

    public WebElement getTrashElement() {
        return Browser.current().getElement(TRASH_BOX_LOCATOR);
    }

    public static WebElement getFileElement() {
        Browser.current().waitForElementIsClickable(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME)));
        return Browser.current().getElement(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME)));
    }

    public static WebElement getFileElements() {
        Browser.current().waitForElementIsClickable(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME_3)));
        return Browser.current().getElement(By.xpath(String.format(FILE_LOCATOR_PATTERN, FILE_NAME_3)));
    }
    public String getCurrentLogin() {
        return Browser.current().getElement(CURRENT_ADDRESS_ELEMENT).getAttribute("textContent");

    }

}
