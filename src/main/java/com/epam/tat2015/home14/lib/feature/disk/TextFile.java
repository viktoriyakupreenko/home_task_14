package com.epam.tat2015.home14.lib.feature.disk;


import com.epam.tat2015.home14.lib.report.Logger;

public class TextFile {

    private String name;
    private String content;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent() {
        Logger.info("Expected content of downloaded file: " + content);
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

}
