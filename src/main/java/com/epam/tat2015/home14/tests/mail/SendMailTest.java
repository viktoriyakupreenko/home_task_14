package com.epam.tat2015.home14.tests.mail;


import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.mail.Letter;
import com.epam.tat2015.home14.lib.feature.mail.LetterBuilder;
import com.epam.tat2015.home14.lib.feature.mail.service.MailLoginService;
import com.epam.tat2015.home14.lib.feature.mail.service.MailService;
import com.epam.tat2015.home14.lib.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SendMailTest {
    private MailService mailServise = new MailService();
    private MailLoginService loginService = new MailLoginService();
    private Account account = AccountBuilder.getDefaultAccount();
    Letter letter = LetterBuilder.getDefaultLetter();

    @Test(description = "Send mail fill with all fields properly  ")
    public void sendMailTest() {
        loginService.loginToMailbox(account);
        mailServise.sendLetter(letter);
        Assert.assertTrue(mailServise.isLetterSent(), "Mail was not sent");
    }

    @Test(description = "Check that letter is present in inbox folder", dependsOnMethods = "sendMailTest")
    public void isMailInInboxTest() {
        Browser.refresh();
        Assert.assertTrue(mailServise.isLetterExistInInbox(letter));
    }

    @Test(description = "Check that letter is present in sent folder",dependsOnMethods = "sendMailTest")
    public void isMailInSentTest() {
        Browser.refresh();
        Assert.assertTrue(mailServise.isLetterExistInSent(letter));
    }
}
