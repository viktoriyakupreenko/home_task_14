package com.epam.tat2015.home14.lib.feature.mail;

import static com.epam.tat2015.home14.lib.feature.common.CommonConstants.*;

public class LetterBuilder {

    public static Letter getDefaultLetter() {
        Letter letter = new Letter();
        letter.setRecipient(DEFAULT_USER_MAIL);
        letter.setSubject(emailSubject);
        letter.setBody(emailBodyText);
        return letter;
    }

    public static Letter getLetterWithoutRecipient() {
        Letter letter = new Letter();
        letter.setSubject(emailSubject);
        letter.setBody(emailBodyText);
        return letter;
    }

    public static Letter getLetterWithoutSubjectBody() {
        Letter letter = new Letter();
        letter.setRecipient(DEFAULT_USER_MAIL);
        return letter;
    }

}
