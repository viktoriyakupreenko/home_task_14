package com.epam.tat2015.home14.tests.disk;

import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.disk.FileBuilder;
import com.epam.tat2015.home14.lib.feature.disk.TextFile;
import com.epam.tat2015.home14.lib.feature.disk.service.DiskLoginService;
import com.epam.tat2015.home14.lib.feature.disk.service.DiskService;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RemoveSeveralElementsTest {
    private DiskService diskService = new DiskService();
    private FileBuilder fileBuilder = new FileBuilder();
    private DiskLoginService loginService = new DiskLoginService();
    private Account account = AccountBuilder.getDefaultAccount();

    @Test(description = "Check files are moved to trash ")
    public void removeSeveralFilesToTrash() {
        loginService.loginToDisk(account);
        TextFile textFile = fileBuilder.createFiles();
        diskService.uploadSeveralFiles();
        diskService.checkFieIsPresent(textFile);
        diskService.removeFiles();
        Assert.assertTrue(diskService.checkFileIsPresentInTrash(textFile), "Files are not in trash");
    }
}
