package com.epam.tat2015.home14.tests.disk;

import com.epam.tat2015.home14.lib.feature.common.Account;
import com.epam.tat2015.home14.lib.feature.common.AccountBuilder;
import com.epam.tat2015.home14.lib.feature.disk.FileBuilder;
import com.epam.tat2015.home14.lib.feature.disk.TextFile;
import com.epam.tat2015.home14.lib.feature.disk.service.DiskLoginService;
import com.epam.tat2015.home14.lib.feature.disk.service.DiskService;
import com.epam.tat2015.home14.lib.ui.Browser;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RestoreTest {
    private DiskService diskService = new DiskService();
    private FileBuilder fileBuilder = new FileBuilder();
    private DiskLoginService loginService = new DiskLoginService();
    private Account account = AccountBuilder.getDefaultAccount();
    TextFile textFile = fileBuilder.createFile();

    @Test(description = "Check file is restored and moved to previous folder ")
    public void restoreFileTest() {
        loginService.loginToDisk(account);
        diskService.uploadFile();
        Browser.refresh();
        diskService.checkFieIsPresent(textFile);
        diskService.removeFile();
        Browser.refresh();
        diskService.restoreFile(textFile);
        diskService.returnToDisk();
        Browser.refresh();
        Assert.assertTrue(diskService.checkFieIsPresent(textFile), "File was not restore to previous folder ");
    }
}
